<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\transaction;

class MyController extends Controller
{
    function getData()
        {
            
            $trans = transaction::get();

            return response()->json($trans, 200);
        }

    function addData(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); //untuk membungkus transaksi, fungsinya adalah jika insert data sukses maka data 
            // akan dicommit dan disave ke database, dan jika gagal maka data akan dirollback

            try
                {
                    //validasi client untuk input
                    $this->validate($request,[
                        'id_customer' => 'required', 
                        'id_room' => 'required'
                        // 'checkin' => 'required',
                        // 'checkout' => 'required',
                    ]);

                    //save ke database
                    $id_customer = $request->input('id_customer'); //data diambil dari client ke server
                    $id_room = $request->input('id_room');
                    $checkin = $request->input('checkin');
                    $checkout = $request->input('checkout');

                    // save ke database menggunakan metode eloquen
                    $trans = new transaction;
                    $trans->id_customer = $id_customer;
                    $trans->id_room = $id_room;
                    $trans->checkin = $checkin;
                    $trans->checkout = $checkout;
                    $trans->save();

                    $transaction = transaction::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($transaction, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }
}
