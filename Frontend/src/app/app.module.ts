import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomDetailsEditComponent } from './room-details-edit/room-details-edit.component';
import { CustomerlistComponent } from './customerlist/customerlist.component';
import { TransactionDetailsEditComponent } from './transaction-details-edit/transaction-details-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    RoomDetailsEditComponent,
    CustomerlistComponent,
    TransactionDetailsEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
